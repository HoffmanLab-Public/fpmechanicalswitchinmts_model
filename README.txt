Code for Solving/Simulating and Analyzing Mathematical Models of FP Mechanical Switching in Load-bearing Proteins and MTSs

Code for ES framework applied to MTS is located here:
TheoreticalESHistogramsForMTS

Code for ODE model of single FP is located here:
2StateSingleFP_ODEModel

Code for ODE model of MTS is located here:
4StateSensor_ODEModel

Code for stochastic model of MTS is located here:
4StateSensor_StochaticSimulations

Code incorporating force-dependent unbinding rate constants into stochastic model of MTS is located here:
Extension_ForceDepBonds\4StateSensor_ODEModel_ForceDepBonds

Code incorporating force-dependent unbinding rate constants into stochastic model of MTS is located here:
Extension_ForceDepBonds\4StateSensor_Stochastic_ForceDepBonds

Code incorporating force-dependent unbinding rate constants AND loading rate control into stochastic model of MTS is located here:
Extension_ForceDepBonds\4StateSensor_Stochastic_LoadRate